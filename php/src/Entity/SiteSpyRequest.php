<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="site_spy_request")
 */
class SiteSpyRequest
{

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $url;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isActive;
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @var DateTimeImmutable $createdAt
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    /**
     * @var DateTimeImmutable $updatedAt
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @param string $url
     * @param string $id
     * @param bool $isActive
     */
    public function __construct(string $url, string $id, bool $isActive = true)
    {
        $this->url = $url;
        $this->id = $id ?? Uidv4;
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    public function deactivate(): void
    {
        $this->isActive = false;
    }


    public function activate(): void
    {
        $this->isActive = true;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

}