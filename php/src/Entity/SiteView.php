<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="site_view")
 */
class SiteView
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;
    /**
     * @var int
     * @ORM\Column(type="string")
     */
    private $code;
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $html;
    /**
     * @var SiteSpyRequest
     * @ORM\ManyToOne(targetEntity="SiteSpyRequest")
     * @ORM\JoinColumn(name="request_id", referencedColumnName="id")
     */
    private $request;

    /**
     * @var DateTimeImmutable $createdAt
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @param string $id
     * @param int $code
     * @param string $html
     * @param SiteSpyRequest $request
     */
    public function __construct(string $id, int $code, string $html, SiteSpyRequest $request)
    {
        $this->id = $id;
        $this->code = $code;
        $this->html = $html;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }

    /**
     * @return SiteSpyRequest
     */
    public function getRequest(): SiteSpyRequest
    {
        return $this->request;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

}