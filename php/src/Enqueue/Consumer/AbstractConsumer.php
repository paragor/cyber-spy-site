<?php


namespace App\Enqueue\Consumer;


use Doctrine\ORM\EntityManagerInterface;
use Enqueue\Client\CommandSubscriberInterface;
use Interop\Queue\Context;
use Interop\Queue\Message;
use Interop\Queue\Processor;
use Psr\Log\LoggerInterface;
use Throwable;

abstract class AbstractConsumer implements Processor, CommandSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager
    ) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    /**
     * The method has to return either self::ACK, self::REJECT, self::REQUEUE string.
     *
     * The method also can return an object.
     * It must implement __toString method and the method must return one of the constants from above.
     *
     * @param Message $message
     * @param Context $context
     *
     * @return string|object with __toString method implemented
     */
    public function process(Message $message, Context $context)
    {
        $this->logger->info('[Start]' . __CLASS__ . '::' . __METHOD__, ['body' => $message->getBody()]);
        try {
            $result = $this->doProcess($message, $context);
        } catch (Throwable $exception) {
            $this->logger->error($exception->getMessage(), ['body' => $message->getBody()]);
            $result = self::REJECT;
        }
        $this->logger->info('[Stop]' . __CLASS__ . '::' . __METHOD__, ['body' => $message->getBody()]);

        if (!$this->entityManager->isOpen()) {
            throw new \RuntimeException('Database connection closed');
        }
        return $result;
    }

    /**
     * @param Message $message
     * @param Context $context
     *
     * @return string self::ACK self::REJECT self::REQUEUE
     */
    abstract public function doProcess(Message $message, Context $context): string;
}