<?php


namespace App\Enqueue\Consumer;


use App\Repository\Exception\CantSaveRequestException;
use App\Repository\RequestsRepositoryInterface;
use App\Repository\SiteViewsRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Interop\Queue\Context;
use Interop\Queue\Message;
use Psr\Log\LoggerInterface;
use Throwable;

class ParseSiteConsumer extends AbstractConsumer
{
    /**
     * @var SiteViewsRepositoryInterface
     */
    private $viewsRepository;
    /**
     * @var RequestsRepositoryInterface
     */
    private $requestsRepository;
    /**
     * @var ClientInterface
     */
    private $guzzle;

    /**
     * @param SiteViewsRepositoryInterface $viewsRepository
     * @param RequestsRepositoryInterface $requestsRepository
     * @param ClientInterface $guzzle
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        SiteViewsRepositoryInterface $viewsRepository,
        RequestsRepositoryInterface $requestsRepository,
        ClientInterface $guzzle,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($logger, $entityManager);
        $this->viewsRepository = $viewsRepository;
        $this->requestsRepository = $requestsRepository;
        $this->guzzle = $guzzle;
    }

    /**
     * @return string|array
     */
    public static function getSubscribedCommand()
    {
        return ['command' => 'parse'];
    }


    /**
     * @param Message $message
     * @param Context $context
     *
     * @return string
     */
    public function doProcess(Message $message, Context $context): string
    {
        $requestId = $message->getBody();
        $request = $this->requestsRepository->findRequestById($requestId);
        $loggerContext = ['requestId' => $requestId];
        if ($request === null) {
            $this->logger->error('Cant find request with id: ' . $requestId, $loggerContext);
            return self::REJECT;
        }
        $loggerContext['requestUrl'] = $request->getUrl();
        try {
            $response = $this->guzzle->request('GET', $request->getUrl());
        } catch (Throwable|GuzzleException $exception) {
            $this->logger->error('Cant download: ' . $exception->getMessage(), $loggerContext);
            return self::REJECT;
        }
        $html = $response->getBody()->getContents();
        $code = $response->getStatusCode();
        $view = $this->viewsRepository->createSiteView($html, $code, $request);

        $lastView = $this->viewsRepository->findLastSiteViewBySiteSpyRequest($request);
        if ($lastView !== null && $view->getHtml() === $lastView->getHtml()) {
            $this->logger->info('Last view has the same html', $loggerContext);
            return self::ACK;
        }

        try {
            $this->viewsRepository->saveSiteView($view);
            $this->logger->info('Save view: ' . $view->getId(), $loggerContext);
        } catch (CantSaveRequestException $exception) {
            $this->logger->error('Cant save view: ' . $exception->getMessage(), $loggerContext);
            return self::REJECT;
        }
        return self::ACK;
    }
}