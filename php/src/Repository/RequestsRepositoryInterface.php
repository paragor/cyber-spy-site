<?php

namespace App\Repository;

use App\Entity\SiteSpyRequest;
use App\Repository\Exception\CantSaveRequestException;

interface RequestsRepositoryInterface
{

    /**
     * @param string $url
     *
     * @return SiteSpyRequest
     */
    public function createRequest(string $url): SiteSpyRequest;

    /**
     * @param string $id
     *
     * @return SiteSpyRequest|null
     */
    public function findRequestById(string $id): ?SiteSpyRequest;

    /**
     * @param SiteSpyRequest $request
     *
     * @throws CantSaveRequestException
     */
    public function saveRequest(SiteSpyRequest $request): void;

    /**
     * @return SiteSpyRequest[]
     */
    public function findAllActive(): array;

}