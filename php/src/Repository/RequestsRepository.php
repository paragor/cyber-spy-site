<?php

namespace App\Repository;

use App\Entity\SiteSpyRequest;
use App\Repository\Exception\CantSaveRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Ramsey\Uuid\Uuid;

class RequestsRepository implements RequestsRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $url
     *
     * @return SiteSpyRequest
     * @throws Exception
     */
    public function createRequest(string $url): SiteSpyRequest
    {
        $id = Uuid::uuid4()->toString();
        return new SiteSpyRequest($url, $id, true);
    }

    /**
     * @param string $id
     *
     * @return SiteSpyRequest|null
     */
    public function findRequestById(string $id): ?SiteSpyRequest
    {
        return $this->entityManager->getRepository(SiteSpyRequest::class)->find($id);
    }

    /**
     * @param SiteSpyRequest $request
     *
     * @throws CantSaveRequestException
     */
    public function saveRequest(SiteSpyRequest $request): void
    {
        try {
            $this->entityManager->persist($request);
            $this->entityManager->getUnitOfWork()->commit($request);
        } catch (Exception $exception) {
            throw new CantSaveRequestException($exception->getMessage(), 0, $exception);
        }
    }

    /**
     * @return SiteSpyRequest[]
     */
    public function findAllActive(): array
    {
        return $this->entityManager
            ->createQuery('SELECT r FROM ' . SiteSpyRequest::class . ' r where r.isActive = true')
            ->execute();
    }
}