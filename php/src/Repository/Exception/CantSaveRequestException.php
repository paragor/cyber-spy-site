<?php

namespace App\Repository\Exception;

use App\Exception\AppException;

class CantSaveRequestException extends AppException
{

}