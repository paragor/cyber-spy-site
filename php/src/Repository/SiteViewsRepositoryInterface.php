<?php

namespace App\Repository;

use App\Entity\SiteSpyRequest;
use App\Entity\SiteView;
use App\Repository\Exception\CantSaveRequestException;

interface SiteViewsRepositoryInterface
{

    /**
     * @param string $html
     * @param int $code
     * @param SiteSpyRequest $request
     *
     * @return SiteView
     */
    public function createSiteView(string $html, int $code, SiteSpyRequest $request): SiteView;

    /**
     * @param string $id
     *
     * @return SiteSpyRequest|null
     */
    public function findSiteView(string $id): ?SiteView;

    /**
     * @param SiteSpyRequest $request
     *
     * @return SiteView[]
     */
    public function findSiteViewsBySiteSpyRequest(SiteSpyRequest $request): array;

    /**
     * @param SiteView $view
     *
     * @throws CantSaveRequestException
     */
    public function saveSiteView(SiteView $view): void;

    /**
     * @param SiteSpyRequest $request
     *
     * @return SiteView|null
     */
    public function findLastSiteViewBySiteSpyRequest(SiteSpyRequest $request): ?SiteView;

}