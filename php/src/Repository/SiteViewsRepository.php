<?php

namespace App\Repository;

use App\Entity\SiteSpyRequest;
use App\Entity\SiteView;
use App\Repository\Exception\CantSaveRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Ramsey\Uuid\Uuid;

class SiteViewsRepository implements SiteViewsRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $id
     *
     * @return SiteSpyRequest|null
     */
    public function findSiteView(string $id): ?SiteView
    {
        return $this->entityManager->getRepository(SiteView::class)->find($id);
    }

    /**
     * @param SiteSpyRequest $request
     *
     * @return SiteView[]
     */
    public function findSiteViewsBySiteSpyRequest(SiteSpyRequest $request): array
    {
        return $this->entityManager
            ->createQuery('Select v from ' . SiteView::class . ' v where v.request = :request order by v.createdAt')
            ->setParameter('request', $request)
            ->execute();
    }

    /**
     * @param SiteSpyRequest $request
     *
     * @return SiteView|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastSiteViewBySiteSpyRequest(SiteSpyRequest $request): ?SiteView
    {
        return $this->entityManager
            ->createQuery('Select v from ' . SiteView::class . ' v where v.request = :request order by v.createdAt desc')
            ->setParameter('request', $request)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @param SiteView $view
     *
     * @throws CantSaveRequestException
     */
    public function saveSiteView(SiteView $view): void
    {
        try {
            $this->entityManager->persist($view);
            $this->entityManager->getUnitOfWork()->commit($view);
        } catch (Exception $exception) {
            throw new CantSaveRequestException($exception->getMessage(), 0, $exception);
        }
    }

    /**
     * @param string $html
     * @param int $code
     * @param SiteSpyRequest $request
     *
     * @return SiteView
     * @throws Exception
     */
    public function createSiteView(string $html, int $code, SiteSpyRequest $request): SiteView
    {
        $id = Uuid::uuid4()->toString();
        return new SiteView($id, $code, $html, $request);
    }
}