<?php

namespace App\Console;

use App\Repository\RequestsRepositoryInterface;
use Enqueue\Client\ProducerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StartParsingCommand extends Command
{

    /**
     * @var ProducerInterface
     */
    private $producer;
    /**
     * @var RequestsRepositoryInterface
     */
    private $requestsRepository;


    public function __construct(RequestsRepositoryInterface $requestsRepository, ProducerInterface $producer)
    {
        parent::__construct();
        $this->requestsRepository = $requestsRepository;
        $this->producer = $producer;
    }

    protected function configure()
    {
        $this
            ->setName('parsing:start')
            ->setDescription('Запускает парсинг');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $requests = $this->requestsRepository->findAllActive();
        foreach ($requests as $request) {
            $this->producer->sendCommand(
                'parse',
                $request->getId()
            );
            $output->writeln("Send {$request->getUrl()} (id: {$request->getId()})");
        }
        $output->writeln('done');
    }
}
