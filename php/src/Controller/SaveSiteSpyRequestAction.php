<?php

namespace App\Controller;

use App\Repository\Exception\CantSaveRequestException;
use App\Repository\RequestsRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment as Twig;

class SaveSiteSpyRequestAction
{
    /**
     * @var Twig
     */
    private $twig;
    /**
     * @var RequestsRepositoryInterface
     */
    private $requestsRepository;

    public function __construct(Twig $twig, RequestsRepositoryInterface $requestsRepository)
    {
        $this->twig = $twig;
        $this->requestsRepository = $requestsRepository;
    }

    /**
     * @Route("/", name="save_spy_site_request", methods={"POST"})
     * @param Request $request
     *
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __invoke(Request $request)
    {
        $url = $request->request->get('url');
        if (!preg_match('!https?://!', $url)) {
            return new Response($this->twig->render('index.html.twig', ['answer' => 'BAD URL']));
        }
        $spyRequest = $this->requestsRepository->createRequest($url);
        try {
            $this->requestsRepository->saveRequest($spyRequest);
        } catch (CantSaveRequestException $exception) {
            return new Response($this->twig->render('index.html.twig', ['answer' => 'PROBLEMS WITH SAVING: ' . $exception->getMessage()]));
        }
        return new Response($this->twig->render('index.html.twig', ['answer' => 'OK']));
    }

}