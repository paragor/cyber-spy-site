<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class IndexAction
{
    /**
     * @var Environment
     */
    private $engine;

    public function __construct(Environment $twig)
    {
        $this->engine = $twig;
    }

    /**
     * @Route("/", name="main", methods={"GET"})
     */
    public function __invoke()
    {
        return new Response($this->engine->render('index.html.twig'));
    }

}